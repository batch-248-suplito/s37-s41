//dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

//import route
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

//server
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//connect to mongodb atlas
const DATABASE_USERNAME = 'admin';
const DATABASE_PASSWORD = 'admin123';
const DATABASE_NAME = 'booking-api';

mongoose.set('strictQuery', true);
mongoose.connect(`mongodb+srv://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@clusterbatch248.riqw3pg.mongodb.net/${DATABASE_NAME}?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

//connecting to mongodb database
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log('Hi! we are connected to MongoDB Atlas!'));

//route endpoints
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

//server listening
app.listen(process.env.PORT || 4000, () => console.log(`Api is now online on port ${process.env.PORT || 4000}`));