//dependencies
const express = require('express');
const router = express.Router();

//import controller
const userController = require('../controllers/userControllers');
const auth = require("../auth")

router.post('/checkEmail', (req,res) => {
    userController.checkEmailExists(req.body).then(resultFromUserController => res.send(resultFromUserController));
});

router.post('/register', (req,res) => {
    userController.registerUser(req.body).then(resultFromUserController => res.send(resultFromUserController));
});

//authentication
router.post('/login', (req,res) => {
    userController.loginUser(req.body).then(resultFromUserController => res.send(resultFromUserController));
});

//ACTIVITY 1. Create a ldetails route that will accept the user's Id to retrieve the details of a user.
//ACTIVITY 2. Create a getProfile controller method for retrieving the details of the user:
// a. Find the document in the database using the user's ID
// b. Reassign the password of the returned document to an empty string
// c. Return the result back to the frontend
// router.post('/details', (req,res) => {
//     userController.getProfile(req.body).then(resultFromUserController => res.send(resultFromUserController));
// });
//solution
router.get("/details", auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromUserController=>res.send(resultFromUserController));

});

//enroll a user
router.post("/enroll", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = {
        userId: userData,
        courseId: req.body.courseId
    }
    userController.enroll(data).then(resultFromUserController => res.send(resultFromUserController));

});


module.exports = router;