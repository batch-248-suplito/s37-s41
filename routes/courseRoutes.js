//dependencies
const express = require('express');
const router = express.Router();
const auth = require("../auth")

//import controller
const courseController = require('../controllers/courseControllers');

//verify admin
router.post('/', auth.verify, (req,res) => {

    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    courseController.addCourse(data).then(resultFromCourseController => res.send(resultFromCourseController));
});

//get all courses
router.get('/all', (req,res) => {
    courseController.getAllCourses().then(resultFromCourseController => res.send(resultFromCourseController));
})

//get all active
router.get('/', (req,res) => {
    courseController.getAllActive().then(resultFromCourseController => res.send(resultFromCourseController));
})

//update a course
router.put('/:courseId', auth.verify, (req,res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromCourseController => res.send(resultFromCourseController));
});

//ACTIVITY update using patch with body
// router.patch('/:courseId/archive', auth.verify, (req,res) => {
//     courseController.archiveCourse(req.params, req.body).then(resultFromCourseController => res.send(resultFromCourseController));
// });

//ACTIVITY update using patch w/o body
router.patch('/:courseId/archive', auth.verify, (req,res) => {
    courseController.archiveCourse(req.params).then(resultFromCourseController => res.send(resultFromCourseController));
});

//ACTIVITY update using patch with body
// router.patch('/:courseId/unarchive', auth.verify, (req,res) => {
//     courseController.unArchiveCourse(req.params, req.body).then(resultFromCourseController => res.send(resultFromCourseController));
// });

//ACTIVITY update using patch w/o body
router.patch('/:courseId/unarchive', auth.verify, (req,res) => {
    courseController.unArchiveCourse(req.params).then(resultFromCourseController => res.send(resultFromCourseController));
});

module.exports = router;