//import routes
const User = require('../models/User');
const Course = require('../models/Course');
const auth = require('../auth');
const bcrypt = require('bcrypt');

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if (result.length > 0) {
            return true;
        } else {
            return false;
        }
    })
};

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user ,error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    })
};

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if (result == null) {
            return false;
        } else {
            const isPasswordcorrect = bcrypt.compareSync(reqBody.password, result.password);
            if (isPasswordcorrect) {
                return {access: auth.createAccessToken(result)};
            } else {
                return false;
            }
        }
    })
};

//ACTIVITY 1. Create a ldetails route that will accept the user's Id to retrieve the details of a user.
//ACTIVITY 2. Create a getProfile controller method for retrieving the details of the user:
// a. Find the document in the database using the user's ID
// b. Reassign the password of the returned document to an empty string
// c. Return the result back to the frontend
// module.exports.getProfile = (reqBody) => {
//     return User.findById({_id: reqBody.id}).then((userById, err) => {
//         if (!err) {
//             userById.password = "";
//             return userById;
//         }
//     })
// };
// solution
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result=>{
		result.password = "";
		return result;
	})
};

//enroll a user
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId.id).then(user => {
        user.enrollments.push({courseId: data.courseId});
        return user.save().then((user, error) => {
            if (error) {
                return false;
            } else {
                return true;
            };
        });
    });

    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        course.enrollees.push({userId: data.userId.id});
        return course.save().then((course,error) => {
            if (error) {
                return false;
            } else {
                return true;
            };
        });
    });

    if (isUserUpdated && isCourseUpdated) {
        return true;
    } else {
        return false;
    };

};