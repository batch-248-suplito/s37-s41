//import routes
const Course = require('../models/Course');

//add new courses
module.exports.addCourse = (data) => {

    if (data.isAdmin) {

        let newCourse = new Course({
            name: data.course.name,
            description: data.course.description,
            price: data.course.price
        });
    
        return newCourse.save().then((course, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        })

    } else {
        return false;
    }
}

//get all courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => result);
}

// get all active
module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => result);
}

//update a course
module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    })
}

//ACTIVITY update using patch with body
// module.exports.archiveCourse = (reqParams, reqBody) => {
//     let patchCourse = {
//         isActive: reqBody.isActive
//     }

//     return Course.findByIdAndUpdate(reqParams.courseId, patchCourse).then((course, error) => {
//         if (error) {
//             return false;
//         } else {
//             return true;
//         }
//     })
// }

//ACTIVITY update using patch w/o body
module.exports.archiveCourse = (reqParams) => {
    return Course.findByIdAndUpdate(reqParams.courseId, {isActive: false}).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    })
}

//ACTIVITY update using patch with body
// module.exports.unArchiveCourse = (reqParams, reqBody) => {
//     let patchCourse = {
//         isActive: reqBody.isActive
//     }

//     return Course.findByIdAndUpdate(reqParams.courseId, patchCourse).then((course, error) => {
//         if (error) {
//             return false;
//         } else {
//             return true;
//         }
//     })
// }

//ACTIVITY update using patch w/o body
module.exports.unArchiveCourse = (reqParams) => {
    return Course.findByIdAndUpdate(reqParams.courseId, {isActive: true}).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    })
}